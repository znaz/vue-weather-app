import { createStore } from 'vuex'
import VuexPersist from 'vuex-persist'

const vuexLocalStorage = new VuexPersist({
  key: 'vuex',
  storage: window.localStorage,
  reducer: (state) => ({
    searchTerm: state.searchTerm,
    user: state.user,
    savedWeather: state.savedWeather,
    locationWeather: state.locationWeather,
    city: state.city
  })
})

const store = createStore({
  state() {
    return {
      searchTerm: '',
      loading: false,
      error: null,
      weatherData: null,
      user: null,
      city: null,
      savedWeather: [],
      locationWeather: null
    }
  },
  mutations: {
    setSearchTerm(state, term) {
      state.searchTerm = term
    },
    setLoading(state, value) {
      state.loading = value
    },
    setError(state, error) {
      state.error = error
    },
    setCity(state, value) {
      state.city = value
    },
    setWeatherData(state, weatherData) {
      state.weatherData = weatherData
    },
    addUser(state, user) {
      state.user = user
    },
    removeUser(state) {
      state.user = null
    },
    addSavedWeather(state, weather) {
      state.savedWeather.push(weather)
    },
    setLocationWeather(state, weather) {
      state.locationWeather = weather
    }
  },
  actions: {
    saveUser({ commit }, user) {
      commit('addUser', user)
    },
    saveCity({ commit }, city) {
      commit('addCity', city)
    },
    removeUser({ commit }) {
      commit('removeUser')
    },
    saveWeatherData({ commit }, weather) {
      commit('addSavedWeather', weather)
    },
    saveLocationWeather({ commit }, weather) {
      commit('setLocationWeather', weather)
    }
  },
  plugins: [vuexLocalStorage.plugin]
})

export default store
