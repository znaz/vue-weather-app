import axios from 'axios'

const API_KEY = 'e9ae32895dbade34cea8a040276478d6'
const API_URL = 'https://api.openweathermap.org/data/2.5/weather'
export async function getWeatherData(city) {
  try {
    const response = await axios.get(API_URL, {
      params: {
        q: city,
        appid: API_KEY,
        units: 'metric'
      }
    })
    return response.data
  } catch (error) {
    throw new Error('Failed to fetch weather data')
  }
}
export async function getForecastWeatherData(cityName) {
  try {
    const response = await axios.get(
      `http://api.openweathermap.org/data/2.5/forecast?q=${cityName.value}&appid=${API_KEY}`
    )

    const forecastWeatherData = response.data
    console.log(forecastWeatherData)
    return forecastWeatherData
  } catch (error) {
    throw new Error('Failed to fetch forecast weather data')
  }
}
