import { createRouter, createWebHistory } from 'vue-router'
import Layout from '../components/RootLayout.vue'
import Home from '../views/HomePage.vue'
import Login from '../views/LoginPage.vue'
import Signup from '../views/SignupPage.vue'
import MyWeather from '../views/SavedWeathers.vue'
import Forecast from '../views/ForecastWeather.vue'

const routes = [
  {
    path: '/',
    component: Layout,
    children: [
      {
        path: '/',
        name: 'Home',
        component: Home
      },
      {
        path: '/saved',
        component: MyWeather
      },
      {
        path: '/forecast',
        component: Forecast
      }
    ]
  },
  {
    path: '/login',
    component: Login
  },
  {
    path: '/signup',
    component: Signup
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
