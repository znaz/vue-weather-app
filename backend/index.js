const express = require("express");
const mongoose = require("mongoose");
const app = express();
const cookieParser = require("cookie-parser");
const cors = require("cors");
require("dotenv").config();
const port = process.env.PORT || 3000;




app.use(
  cors({
    origin: ["http://localhost:5173"],
    methods: ["GET", "POST", "PUT", "PATCH", "DELETE"],
    credentials: true,
  })
);
app.use(express.json());
app.use(cookieParser());



const Users = require("./Routes/user")
const Weather = require("./Routes/weather")

app.use("/users", Users )
app.use("/weathers", Weather)

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});

main()
  .then((res) => console.log("db connected"))
  .catch((err) => console.log(err));

async function main() {
  const dbUrl = process.env.DB_URL;
  const dbPassword = process.env.DB_PASSWORD;
  const UrlWithPassword = dbUrl.replace("<password>", dbPassword);
  await mongoose.connect(UrlWithPassword);
}
