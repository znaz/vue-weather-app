const express = require('express');
const router = express.Router();
const weatherController = require('../Controllers/weather');


router.post('/add-weather', weatherController.addWeather);
router.get('/api/:userId', weatherController.getWeatherByUser);
router.delete('/api/:weatherId', weatherController.deleteWeather)


module.exports = router;