const mongoose = require("mongoose");

const weathersSchema = new mongoose.Schema(
  {
    city: {
      type: String,
      required: true,
    },
    temperature: {
      type: Number,
      required: true,
    },
    weather: {
      type: String,
      required: true,
    },
    weatherIcon: {
      type: String,
      required: true,
    },
    user: {
      type: mongoose.ObjectId,
      ref: "User",
    },
  },
  { timestamps: true }
);
const Weather = mongoose.model("Weather", weathersSchema);

module.exports = Weather;
