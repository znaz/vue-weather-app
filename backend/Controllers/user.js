const User = require("../Models/user")
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

const signUp = async (req, res, next) => {
  try {
    const { userName, email, password } = req.body;

    const existingUser = await User.findOne({email});

    if (existingUser) {
      return res.status(400).json({
        message: "User with the same username or email already exists",
      });
    }

    const saltRounds = 10;
    const hash = bcrypt.hashSync(password, saltRounds);

    const newUser = new User({
      userName,
      email,
      password: hash,
    });

    await newUser.save();

    res.status(201).json({ message: "User created successfully" });
  } catch (error) {
    console.error(error);
    res.status(500).send("Error Occurred");
  }
};

const login = async (req, res, next) => {
  try {
    const { email, password } = req.body;

    const user = await User.findOne({ email });

    if (!user) {
      return res.status(400).json({
        message: "Incorrect Email or password",
      });
    }
    const checkPassword = bcrypt.compareSync(password, user.password);

    if (!checkPassword) {
      return res.status(400).json({
        message: "Incorrect Email or password",
      });
    }

    const token = jwt.sign(
      { id: user._id, userName: user.userName },
      process.env.JWT_SECRET
    );
    res.cookie("token", token, {
      withCredentials: true,
      httpOnly: true,
    });
    const currentUser = {
      userName: user.userName,
      profilePicture: user.profilePicture,
      email: user.email,
      admin: user.isAdmin,
      _id: user._id,
    };

    res.status(201).json({ currentUser });
  } catch (error) {
    console.error(error);
    res.status(500).send("Error Occurred");
  }
};

const logout = async (req, res, next) => {
  try {
    res.cookie("token", "", { expires: new Date(0) });
    res.status(204).send("Logout successfully");
  } catch (error) {
    console.error(error);
    res.status(500).send("Error Occurred");
  }
};

module.exports = {
  signUp,
  login,
  logout,
};
