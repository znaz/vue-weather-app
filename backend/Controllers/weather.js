const Weather = require("../Models/weather");

const addWeather = async (req, res, next) => {
  try {
    const weather = new Weather(req.body);
    await weather.save();
    res.status(201).json(weather);
  } catch (err) {
    console.log(err);
    res.status(500).send('Error Occurred');
  }
};

const getWeatherByUser = async (req, res, next) => {
  try {
    const userId = req.params.userId;
    const weathers = await Weather.find({ user: userId });
    res.status(200).json(weathers);
  } catch (err) {
    console.log(err);
    res.status(500).send("Error Occurred");
  }
};
const deleteWeather = async (req, res, next) => {
  try {
    const weatherId = req.params.weatherId;
    await Weather.findByIdAndDelete( weatherId );
    res.status(200).json("deleted");
  } catch (err) {
    console.log(err);
    res.status(500).send("Error Occurred");
  }
};


module.exports = {
    addWeather, getWeatherByUser,deleteWeather
}